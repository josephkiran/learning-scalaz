import scalaz._
import Scalaz._

val list = List(1,2,3,4)

//syntactic sugar
(1 == 2) ? 'a' | 'b'
1 == 2 option 'a'
1.some === none[Int] // typesafe option
'a' =/= 'b'
1.some | 2  //getOrElse
'a'.some |+| none

//List is a Semigroup
list |+| list   // Monadic append aka mappend
Map(1 -> 1) |+| Map(1 -> 2)

//List is a Monoid
list.isMZero
mzero[List[Int]] |+| list

//List is a Functor
list.map(_ + 1)
(1, 2, 3) map {_ + 1} //applied only to the last element of the tuple

//List is an Applicative Functor
^(list, list) {_ |+| _}
(list |@| list){_ |+| _}  // Applicative Builder

//List is also a Monad
List(1.some , 2.some).flatMap(_ |+| 3.some)

// Utility functions
list.foldMap(_ + 1)
List(list, List(5,6,7,8)).join
list.cojoin

//Lifting a function
val f = Functor[List].lift( (_:Int) * 2)
f(List(1,2,3))  // a -> b => fa -> fb  flatmap does this.

//<*> is a special kind of flatmap which takes a functor and another functor with a function inside it.
3.some <*> {(_: Int) + 5}.some

//Writer
3.set("three").run

(1 |-> 50) filter { x => x.shows contains '7' }